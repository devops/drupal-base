FROM bitnami/drupal
LABEL maintainer "Derrek Croney <derrek.croney@duke.edu>"

## Change user to perform privileged actions
USER 0

## Install 'vim'
RUN install_packages vim
RUN install_packages emacs-nox
RUN install_packages wget
RUN install_packages bash-completion
RUN install_packages rsync
RUN install_packages tar

RUN cd /tmp
RUN apt update && apt-get install -y net-tools

COPY bash.bashrc /etc/

## Install packages for compiling Drupal themes
RUN install_packages npm
RUN npm install -g yarn

WORKDIR /opt/bitnami/drupal
RUN composer require drupal/radix

# Install radix_layouts
WORKDIR /opt/bitnami/drupal/modules/contrib

RUN wget "https://ftp.drupal.org/files/projects/radix_layouts-8.x-4.1.tar.gz" \
      && tar xfvz radix_layouts-8.x-4.1.tar.gz \
      && rm radix_layouts-8.x-4.1.tar.gz

RUN sed -i 's/^core: '\''8.x'\''/core_version_requirement: ^8.7.7 || ^9.0/' \
      /opt/bitnami/drupal/modules/contrib/radix_layouts/radix_layouts.info.yml

# Copy our specific custom themes and modules.
COPY modules/ /opt/bitnami/drupal/modules/
COPY themes/ /opt/bitnami/drupal/themes/
RUN cd /opt/bitnami/drupal/themes/custom/drupal9_dulcet && ls -la

# This command may be better suited in a script residing in the 
# /docker-entrypoint-init.d directory (see below)
# TODO: review
WORKDIR /opt/bitnami/drupal/themes/custom/drupal9_dulcet
RUN yarn && yarn dev

RUN chmod -R a+rwx /opt/bitnami/drupal/modules \
      && chmod -R a+rwx /opt/bitnami/drupal/themes

# This is the location where our migrated files 
# (nodes, taxonomy, managed_files) will be.
ENV DUL_DRUPAL7_EXPORT_DIR "/opt/dul/drupal7-export"
RUN mkdir -p ${DUL_DRUPAL7_EXPORT_DIR}
COPY drupal7-export/ /opt/dul/drupal7-export/

# bitnami/drupal image offers a way to enable additional Drupal modules
ENV DRUPAL_ENABLE_MODULES "components basicshib radix_layouts dul_ingest"

# However, it does not offer a way to enable themes, so...
#
# Copy Drupal-related post-init scripts into the image
# Currently, this includes drush tasks to enable 
# the "Radix" & "Dulcet" themes.
WORKDIR /docker-entrypoint-init.d
COPY docker-entrypoint-init.d ./

WORKDIR /opt/bitnami/drupal

# Revert back to non-root user
USER 1001

## Enable mod_ratelimit module
RUN sed -i -r 's/#LoadModule ratelimit_module/LoadModule ratelimit_module/' /opt/bitnami/apache/conf/httpd.conf

## Modify the ports used by Apache by default
# It is also possible to change these environment variables at runtime
# ENV APACHE_HTTP_PORT_NUMBER=8181
# ENV APACHE_HTTPS_PORT_NUMBER=8143

# Set the shell to "bash"
ENV SHELL /bin/bash
