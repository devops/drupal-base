{
   "node__body" : {
      "deleted" : 0,
      "body_format" : "full_html",
      "langcode" : "und",
      "revision_id" : 17002,
      "body_value" : "<p dir=\"ltr\">The Rubenstein Library can make reproductions from our collections for research or publication, even if you aren’t able to visit our library in person.</p><ul><li>You need to have an <a href=\"https://library.duke.edu/rubenstein/research/register\">online researcher account </a>to request reproductions. You don’t need to be affiliated with Duke to have an account.</li><li>All reproduction orders may <a href=\"https://library.duke.edu/rubenstein/research/reproductions\">incur a fee</a>, which is based on the type of material and the reproduction quality requested. You will be able to pay online with a Mastercard or Visa.</li><li>Most reproductions are delivered 8 to 10 weeks after you have submitted payment.</li><li>See <a href=\"https://library.duke.edu/rubenstein/research/reproductions/request-reading-room\">Making a Reading Room Reproduction Request</a> if you want to request a reproduction of material you have previously used in the reading room.</li></ul><h2 dir=\"ltr\">Placing your reproduction request</h2><p dir=\"ltr\">You can begin the process from the collection guide or the catalog record for the item you want to have reproduced:</p><p></p><div class=\"col-md-8\"><div class=\"table-responsive\"><table border:=\"\" 1px=\"\" solid=\"\" 000000=\"\" frame=\"border\" class=\"table table-bordered\" style=\"width: 750px;\" border=\"1\" align=\"center\"><tbody><tr valign=\"middle\" align=\"center\"><td style=\"border: 1px solid #000000;\" valign=\"middle\" align=\"center\"><p style=\"text-align: center;\">From a <strong>collection guide</strong>:</p><p style=\"text-align: center;\">Click the \"Request\" button on the left:</p><p style=\"text-align: center;\"><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/request_FA.JPG\" alt=\"finding aid request button screen capture\" width=\"225\" height=\"41\" style=\"display: block; margin-left: auto; margin-right: auto;\"></p></td><td style=\"border: 1px solid #000000;\" valign=\"middle\" align=\"center\"><p style=\"text-align: center;\">From a <strong>catalog record</strong>:</p><p style=\"text-align: center;\">Click the \"Request\" button in the top right:</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/catalog_request_button.jpg\" alt=\"catalog request button screen shot\" width=\"225\" height=\"41\" style=\"display: block; margin-left: auto; margin-right: auto;\"></p></td></tr></tbody></table></div></div><p><br clear=\"all\">You will see a list of all the items in the collection. Select the box or volume the material you want to have reproduced is in, and click the “Request” button. If you want to request reproductions from multiple volumes or boxes, you'll need to make a separate request for each:</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/request_list_2021.jpg\" alt=\"screenshot labled to show where box numbers are and where the &quot;request for onsite use&quot; button is\" width=\"1157\" height=\"567\"></p><p>You'll be asked to sign in, and then you'll get a request form with the bibliographic information included. Do not submit the request to view this item in the reading room. Instead, click the red \"Order a Digital Copy\" box located in the top right corner of the page:</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/repro_request_start.jpg\" alt=\"screenshot showing location of link to request a reproduction\" width=\"800\" height=\"350\"><br><br><br> This will take you to the Reproduction Request Form. <em>Once this form is started, you cannot navigate back to the initial request page from this page. To view this item in the reading room, navigate back to the catalog record and <a href=\"https://library.duke.edu/rubenstein/research/requesting-material\">submit a Reading Room Request</a>.</em></p><p dir=\"ltr\">To complete the reproduction request, fill out the <strong>Reproduction Details</strong> and <strong>Copyright Agreement/Terms of Use</strong> portions at the bottom of the form.</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/repro-form.PNG\" alt=\"screenshot of reproduction details form\" width=\"670\" height=\"229\"></p><p dir=\"ltr\">For <strong>Reproduction Details</strong>, complete the following fields:</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Describe your request</em>: Describe the specific material you want reproduced, including folder titles, page numbers, captions, or descriptions of images. Be as specific as possible.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Format</em>: Select the reproduction format you require. Please note that the Rubenstein Library only provides digital copies.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Service Level:</em> Check this box if you are requesting a rush order. Rush orders incur additional charges. If you are requesting a rush order, please complete the deadline field. The Rubenstein Library reserves the right to deny rush requests.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>For publication:</em> Check this box if you plan to publish the reproduction. Publication quality scans may require additional consultation with Rubenstein staff. If you plan on publishing, you will need to submit a <a href=\"https://duke.qualtrics.com/jfe/form/SV_diELQPOwx3OKRql\">permission request form</a>.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Special Requests/Questions:</em> Include any additional information about your order for reproduction staff.</p><p dir=\"ltr\" style=\"padding-left: 30px;\">If you have previously been in contact with a Rubenstein staff member about your request, please check the box and select the name of the staff member.&nbsp;</p><p dir=\"ltr\">For <strong>Copyright Agreement and Terms of Use</strong>, read the notice and check the three boxes below it to acknowledge that you have received a notice of copyright restrictions. If you are requesting additional permissions for collections to which the copyright interests have been transferred to Duke University, please submit a <a href=\"https://duke.qualtrics.com/jfe/form/SV_diELQPOwx3OKRql\">Permissions Request Form</a>. For more information on copyright, please visit our <a href=\"https://library.duke.edu/rubenstein/research/citations-and-permissions\">Citations, Permissions &amp; Copyright page</a>.</p><p dir=\"ltr\">When the form is complete, click Submit Request at the bottom.</p><p dir=\"ltr\">Your request is now submitted, and staff will review your request and contact you if any additional information is needed. Once the order has been confirmed with a staff member, we will send you an invoice with instructions on how to submit your payment online. Most reproduction requests are delivered 8 to 10 weeks after you have submitted payment.</p><p>After the request is submitted, you can go to your <a href=\"https://duke.aeon.atlas-sys.com/logon\">Rubenstein Library account</a> to check the status of your order.</p>",
      "delta" : 0,
      "body_summary" : "",
      "entity_id" : 761,
      "bundle" : "page"
   },
   "langcode" : "und",
   "field_tags" : [],
   "type" : "page",
   "node_access" : null,
   "asidebox" : {
      "block_delta" : null,
      "status" : "disabled",
      "body_value" : null,
      "vid" : 17002,
      "block_module" : null,
      "body_format" : null,
      "nid" : 761,
      "bid" : null,
      "body_title" : ""
   },
   "node_field_data" : {
      "langcode" : "und",
      "default_langcode" : 1,
      "uid" : 1,
      "revision_translation_affected" : 1,
      "created" : 1568141186,
      "sticky" : 0,
      "changed" : null,
      "type" : "page",
      "status" : 1,
      "promote" : 0,
      "title" : "Making a Remote Reproduction Request"
   },
   "revisions" : [
      {
         "timestamp" : 1617897044,
         "node_revision__body" : {
            "entity_id" : 761,
            "bundle" : "page",
            "body_summary" : "",
            "delta" : 0,
            "body_value" : "<p dir=\"ltr\">The Rubenstein Library can make reproductions from our collections for research or publication, even if you aren’t able to visit our library in person.</p><ul><li>You need to have an <a href=\"https://library.duke.edu/rubenstein/research/register\">online researcher account </a>to request reproductions. You don’t need to be affiliated with Duke to have an account.</li><li>All reproduction orders may <a href=\"https://library.duke.edu/rubenstein/research/reproductions\">incur a fee</a>, which is based on the type of material and the reproduction quality requested. You will be able to pay online with a Mastercard or Visa.</li><li>Most reproductions are delivered 8 to 10 weeks after you have submitted payment.</li><li>See <a href=\"https://library.duke.edu/rubenstein/research/reproductions/request-reading-room\">Making a Reading Room Reproduction Request</a> if you want to request a reproduction of material you have previously used in the reading room.</li></ul><h2 dir=\"ltr\">Placing your reproduction request</h2><p dir=\"ltr\">You can begin the process from the collection guide or the catalog record for the item you want to have reproduced:</p><p></p><div class=\"col-md-8\"><div class=\"table-responsive\"><table border:=\"\" 1px=\"\" solid=\"\" 000000=\"\" frame=\"border\" class=\"table table-bordered\" style=\"width: 750px;\" border=\"1\" align=\"center\"><tbody><tr valign=\"middle\" align=\"center\"><td style=\"border: 1px solid #000000;\" valign=\"middle\" align=\"center\"><p style=\"text-align: center;\">From a <strong>collection guide</strong>:</p><p style=\"text-align: center;\">Click the \"Request\" button on the left:</p><p style=\"text-align: center;\"><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/request_FA.JPG\" alt=\"finding aid request button screen capture\" width=\"225\" height=\"41\" style=\"display: block; margin-left: auto; margin-right: auto;\"></p></td><td style=\"border: 1px solid #000000;\" valign=\"middle\" align=\"center\"><p style=\"text-align: center;\">From a <strong>catalog record</strong>:</p><p style=\"text-align: center;\">Click the \"Request\" button in the top right:</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/catalog_request_button.jpg\" alt=\"catalog request button screen shot\" width=\"225\" height=\"41\" style=\"display: block; margin-left: auto; margin-right: auto;\"></p></td></tr></tbody></table></div></div><p><br clear=\"all\">You will see a list of all the items in the collection. Select the box or volume the material you want to have reproduced is in, and click the “Request” button. If you want to request reproductions from multiple volumes or boxes, you'll need to make a separate request for each:</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/request_list_2021.jpg\" alt=\"screenshot labled to show where box numbers are and where the &quot;request for onsite use&quot; button is\" width=\"1157\" height=\"567\"></p><p>You'll be asked to sign in, and then you'll get a request form with the bibliographic information included. Do not submit the request to view this item in the reading room. Instead, click the red \"Order a Digital Copy\" box located in the top right corner of the page:</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/repro_request_start.jpg\" alt=\"screenshot showing location of link to request a reproduction\" width=\"800\" height=\"350\"><br><br><br> This will take you to the Reproduction Request Form. <em>Once this form is started, you cannot navigate back to the initial request page from this page. To view this item in the reading room, navigate back to the catalog record and <a href=\"https://library.duke.edu/rubenstein/research/requesting-material\">submit a Reading Room Request</a>.</em></p><p dir=\"ltr\">To complete the reproduction request, fill out the <strong>Reproduction Details</strong> and <strong>Copyright Agreement/Terms of Use</strong> portions at the bottom of the form.</p><p><img src=\"/rubenstein/sites/default/files/rubenstein/users/kate.collins/repro-form.PNG\" alt=\"screenshot of reproduction details form\" width=\"670\" height=\"229\"></p><p dir=\"ltr\">For <strong>Reproduction Details</strong>, complete the following fields:</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Describe your request</em>: Describe the specific material you want reproduced, including folder titles, page numbers, captions, or descriptions of images. Be as specific as possible.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Format</em>: Select the reproduction format you require. Please note that the Rubenstein Library only provides digital copies.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Service Level:</em> Check this box if you are requesting a rush order. Rush orders incur additional charges. If you are requesting a rush order, please complete the deadline field. The Rubenstein Library reserves the right to deny rush requests.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>For publication:</em> Check this box if you plan to publish the reproduction. Publication quality scans may require additional consultation with Rubenstein staff. If you plan on publishing, you will need to submit a <a href=\"https://duke.qualtrics.com/jfe/form/SV_diELQPOwx3OKRql\">permission request form</a>.</p><p dir=\"ltr\" style=\"padding-left: 30px;\"><em>Special Requests/Questions:</em> Include any additional information about your order for reproduction staff.</p><p dir=\"ltr\" style=\"padding-left: 30px;\">If you have previously been in contact with a Rubenstein staff member about your request, please check the box and select the name of the staff member.&nbsp;</p><p dir=\"ltr\">For <strong>Copyright Agreement and Terms of Use</strong>, read the notice and check the three boxes below it to acknowledge that you have received a notice of copyright restrictions. If you are requesting additional permissions for collections to which the copyright interests have been transferred to Duke University, please submit a <a href=\"https://duke.qualtrics.com/jfe/form/SV_diELQPOwx3OKRql\">Permissions Request Form</a>. For more information on copyright, please visit our <a href=\"https://library.duke.edu/rubenstein/research/citations-and-permissions\">Citations, Permissions &amp; Copyright page</a>.</p><p dir=\"ltr\">When the form is complete, click Submit Request at the bottom.</p><p dir=\"ltr\">Your request is now submitted, and staff will review your request and contact you if any additional information is needed. Once the order has been confirmed with a staff member, we will send you an invoice with instructions on how to submit your payment online. Most reproduction requests are delivered 8 to 10 weeks after you have submitted payment.</p><p>After the request is submitted, you can go to your <a href=\"https://duke.aeon.atlas-sys.com/logon\">Rubenstein Library account</a> to check the status of your order.</p>",
            "langcode" : "und",
            "revision_id" : 17002,
            "body_format" : "full_html",
            "deleted" : 0
         },
         "node_revision__field_tags" : [],
         "sticky" : 0,
         "status" : 1,
         "comment" : 1,
         "field_revision" : {
            "title" : "Making a Remote Reproduction Request",
            "sticky" : 0,
            "changed" : null,
            "status" : 1,
            "default_langcode" : 1,
            "langcode" : null,
            "revision_translation_affected" : 1,
            "created" : null,
            "uid" : 1
         },
         "promote" : 0,
         "title" : "Making a Remote Reproduction Request",
         "log" : "",
         "vuuid" : "01e96914-a820-4074-8377-89c09977b502",
         "uid" : 159
      }
   ],
   "nid" : "761",
   "uuid" : "583f5e7c-d559-4e2c-afb8-550e7eb33645",
   "vid" : 17002,
   "path_alias" : {
      "pid" : 903,
      "alias" : "/research/reproductions/request-remote",
      "langcode" : "und",
      "status" : 0,
      "language" : "und"
   },
   "field_image" : null
}

