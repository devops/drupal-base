{
   "uuid" : "fd330170-95c1-4fa3-9ce2-97adfce3417e",
   "field_image" : null,
   "path_alias" : {
      "langcode" : "und",
      "alias" : "/uarchives/transfer/records-management/destruction",
      "pid" : 312,
      "language" : "und",
      "status" : 0
   },
   "vid" : 5673,
   "nid" : "247",
   "revisions" : [
      {
         "promote" : 0,
         "title" : "Recommendations for Destruction of Non-Permanent Records",
         "field_revision" : {
            "langcode" : null,
            "default_langcode" : 1,
            "uid" : 1,
            "revision_translation_affected" : 1,
            "created" : null,
            "sticky" : 0,
            "status" : 1,
            "changed" : null,
            "title" : "Recommendations for Destruction of Non-Permanent Records"
         },
         "vuuid" : "033ff322-cd55-42a1-ada3-32ab32c05000",
         "uid" : 13,
         "log" : "",
         "timestamp" : 1399510333,
         "comment" : 1,
         "node_revision__field_tags" : [],
         "node_revision__body" : {
            "entity_id" : 247,
            "bundle" : "page",
            "body_summary" : "",
            "delta" : 0,
            "body_value" : "<p>It often becomes impractical or impossible to destroy non-permanent University records using common office shredders. Offices therefore struggle with records destruction methods and may opt to retain information longer than necessary.</p><p>To simplify the process of destroying University records, the Records Management Program offers the following recommendations.</p><h3>When to destroy records:</h3><ul><li>When they have been retained for the length of time specified in records retention guidelines and are not being used in an audit, claim, grievance, or other official action;</li><li>When their usefulness for administrative or operational functions in the office has ended;</li><li>When they do not have permanent historical, legal, or evidential value to the institution; and,</li><li>After a general inventory of what is to be destroyed has been made.</li></ul><h3>When not to destroy records:</h3><ul><li>When they have not been retained for the length of time specified in records retention guidelines;</li><li>When they are being used in an audit, claim, grievance, or other official action;</li><li>When they are still being actively used in the conduct of office business; and,</li><li>When they have permanent historical, legal, or evidential value to the institution.</li></ul><h3>Records destruction should be:</h3><ul><li>Authorized by approved retention guidelines or, in the absence of guidelines, after consulting the University Records Manager;</li><li>Appropriate for the media being destroyed, so that the record is unreadable no matter what its format;</li><li>Timely and routine and done in the normal course of business;</li><li>Secure and confidential so that sensitive information is protected; and,</li><li>Documented in an inventory of what is destroyed, and in a certificate of destruction.</li></ul><h2><strong>Vendors</strong></h2><p>Duke uses a number of recycling and records destruction vendors to securely destroy and/or remove inactive records and media. Contact Procurement Services for information on area service providers.</p><h3>Loose paper</h3><p>If you must routinely destroy large amounts of loose paper, ask a vendor if it can supply bins in which to store the documents. Bins may or may not have locks, so be sure to inquire about locking bins if you handle confidential or sensitive information.</p><h3>Time vs. weight</h3><p>A records destruction vendor may charge by the time it takes to remove and shred the material, or by the material's overall weight. Evaluate the volume of records to be destroyed and resulting costs before deciding on which method to use. Take the time to remove fasteners like large paper clips, binder clips, 3-ring binders, and rubber bands. They add to the weight and must be removed so as not to contaminate the waste paper.</p><h3>Onsite vs. offsite</h3><p>A records destruction vendor may come to your building and shred materials onsite, or it may haul the materials to its own facility and shred them there. Your decision to use onsite or offsite destruction should be based on the volume of records to be destroyed, references, and your potential desire to witness the destruction.</p><h2>Documenting the Destruction</h2><p>Create a clear, concise inventory of the record types to be destroyed. It is not necessary to document what is in every file folder, but collect enough information to answer future questions about what was removed. Examples include:</p><ul><li>financial statements (copies), fy97 &amp; prior, 6 boxes;</li><li><span class=\"end-tag\"></span><span class=\"end-tag\"></span>time sheets, fy99 &amp;<span class=\"entity\"></span>&nbsp;prior, 3 boxes; or,</li><li>applications for vacant position (not hired), fy02 &amp;&nbsp;<span class=\"entity\"></span>prior, 9 boxes.</li></ul><p style=\"margin: 0.75em 0px;\">To further document destruction, make sure a vendor provides a certificate of destruction promptly after destroying the records, and keep the certificate and inventory together.</p><h2>If You Have Questions</h2><p><a href=\"http://library.duke.edu/rubenstein/uarchives/ask\" target=\"_blank\">Contact the Duke University Archives</a>.</p>",
            "langcode" : "und",
            "revision_id" : 5673,
            "deleted" : 0,
            "body_format" : "full_html"
         },
         "sticky" : 0,
         "status" : 1
      }
   ],
   "node_field_data" : {
      "default_langcode" : 1,
      "langcode" : "und",
      "created" : 1378740459,
      "revision_translation_affected" : 1,
      "uid" : 1,
      "sticky" : 0,
      "type" : "page",
      "status" : 1,
      "changed" : null,
      "promote" : 0,
      "title" : "Recommendations for Destruction of Non-Permanent Records"
   },
   "asidebox" : {
      "vid" : 5673,
      "block_module" : null,
      "body_format" : null,
      "block_delta" : null,
      "status" : "disabled",
      "body_value" : null,
      "bid" : null,
      "body_title" : "",
      "nid" : 247
   },
   "type" : "page",
   "field_tags" : [],
   "node_access" : null,
   "node__body" : {
      "body_value" : "<p>It often becomes impractical or impossible to destroy non-permanent University records using common office shredders. Offices therefore struggle with records destruction methods and may opt to retain information longer than necessary.</p><p>To simplify the process of destroying University records, the Records Management Program offers the following recommendations.</p><h3>When to destroy records:</h3><ul><li>When they have been retained for the length of time specified in records retention guidelines and are not being used in an audit, claim, grievance, or other official action;</li><li>When their usefulness for administrative or operational functions in the office has ended;</li><li>When they do not have permanent historical, legal, or evidential value to the institution; and,</li><li>After a general inventory of what is to be destroyed has been made.</li></ul><h3>When not to destroy records:</h3><ul><li>When they have not been retained for the length of time specified in records retention guidelines;</li><li>When they are being used in an audit, claim, grievance, or other official action;</li><li>When they are still being actively used in the conduct of office business; and,</li><li>When they have permanent historical, legal, or evidential value to the institution.</li></ul><h3>Records destruction should be:</h3><ul><li>Authorized by approved retention guidelines or, in the absence of guidelines, after consulting the University Records Manager;</li><li>Appropriate for the media being destroyed, so that the record is unreadable no matter what its format;</li><li>Timely and routine and done in the normal course of business;</li><li>Secure and confidential so that sensitive information is protected; and,</li><li>Documented in an inventory of what is destroyed, and in a certificate of destruction.</li></ul><h2><strong>Vendors</strong></h2><p>Duke uses a number of recycling and records destruction vendors to securely destroy and/or remove inactive records and media. Contact Procurement Services for information on area service providers.</p><h3>Loose paper</h3><p>If you must routinely destroy large amounts of loose paper, ask a vendor if it can supply bins in which to store the documents. Bins may or may not have locks, so be sure to inquire about locking bins if you handle confidential or sensitive information.</p><h3>Time vs. weight</h3><p>A records destruction vendor may charge by the time it takes to remove and shred the material, or by the material's overall weight. Evaluate the volume of records to be destroyed and resulting costs before deciding on which method to use. Take the time to remove fasteners like large paper clips, binder clips, 3-ring binders, and rubber bands. They add to the weight and must be removed so as not to contaminate the waste paper.</p><h3>Onsite vs. offsite</h3><p>A records destruction vendor may come to your building and shred materials onsite, or it may haul the materials to its own facility and shred them there. Your decision to use onsite or offsite destruction should be based on the volume of records to be destroyed, references, and your potential desire to witness the destruction.</p><h2>Documenting the Destruction</h2><p>Create a clear, concise inventory of the record types to be destroyed. It is not necessary to document what is in every file folder, but collect enough information to answer future questions about what was removed. Examples include:</p><ul><li>financial statements (copies), fy97 &amp; prior, 6 boxes;</li><li><span class=\"end-tag\"></span><span class=\"end-tag\"></span>time sheets, fy99 &amp;<span class=\"entity\"></span>&nbsp;prior, 3 boxes; or,</li><li>applications for vacant position (not hired), fy02 &amp;&nbsp;<span class=\"entity\"></span>prior, 9 boxes.</li></ul><p style=\"margin: 0.75em 0px;\">To further document destruction, make sure a vendor provides a certificate of destruction promptly after destroying the records, and keep the certificate and inventory together.</p><h2>If You Have Questions</h2><p><a href=\"http://library.duke.edu/rubenstein/uarchives/ask\" target=\"_blank\">Contact the Duke University Archives</a>.</p>",
      "langcode" : "und",
      "revision_id" : 5673,
      "deleted" : 0,
      "body_format" : "full_html",
      "entity_id" : 247,
      "bundle" : "page",
      "body_summary" : "",
      "delta" : 0
   },
   "langcode" : "und"
}

