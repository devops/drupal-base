{
   "node_access" : null,
   "asidebox" : null,
   "nid" : "357",
   "field_tags" : [],
   "node_field_data" : {
      "type" : "research_page",
      "changed" : null,
      "promote" : 0,
      "langcode" : "und",
      "uid" : 1,
      "revision_translation_affected" : 1,
      "title" : "Open Access at Duke University",
      "status" : 0,
      "created" : 1378476539,
      "sticky" : 0,
      "default_langcode" : 1
   },
   "path_alias" : {
      "status" : 0,
      "langcode" : "und",
      "language" : "und",
      "pid" : 540,
      "alias" : "/research/openaccess"
   },
   "field_image" : null,
   "type" : "research_page",
   "node__body" : {
      "langcode" : "und",
      "body_summary" : "",
      "entity_id" : 357,
      "body_format" : "full_html",
      "bundle" : "research_page",
      "delta" : 0,
      "deleted" : 0,
      "revision_id" : 18365,
      "body_value" : "<h2>What is Open Access?</h2><p><a href=\"https://en.wikipedia.org/wiki/Open_access\">Open access</a> (OA) is the practice of providing unrestricted access via the Internet to peer-reviewed scholarly research. It is most commonly applied to scholarly journal articles, but it is also increasingly being provided to theses, scholarly monographs and book chapters.</p><p>&nbsp;</p><p>At Duke, one of the signature themes of our mission is putting knowledge in the service of society. This means making the fruits of Duke research available as broadly as possible&nbsp;— not just to researchers at places like Duke that have subscription access to scholarly literature via their libraries but to anyone who might benefit from the scholarship being done here.</p><p>Duke University supports open access through a number of initiatives and encourages Duke scholars to work toward making their research openly accessible.</p><h2>Support for Open Access at Duke University</h2><h3>Open Access Policy for Faculty Scholarship</h3><p>In March 2010, the Duke University <a href=\"https://academiccouncil.duke.edu/\">Academic Council</a> <a href=\"https://today.duke.edu/2010/03/accessvote.html\">adopted an open access policy</a> that applies to all Duke faculty members and, unless individual authors choose to opt-out, provides Duke a license to make scholarly articles authored by Duke faculty freely available via a Duke University Libraries repository known as&nbsp;<a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/2841\" style=\"text-decoration: underline;\">DukeSpace</a>. The text of the policy is found in <a href=\"https://provost.duke.edu/wp-content/uploads/FHB_App_P.pdf\">Appendix P</a> of the <a href=\"https://provost.duke.edu/faculty-resources/faculty-handbook/\">Faculty Handbook</a>.&nbsp;If your publisher requests a formal letter waiving the faculty open access policy (i.e., asking you to opt out), please send an email to <a href=\"mailto:open-access@duke.edu\">open-access@duke.edu</a> that includes the name of the publisher and the citation of the article.</p><p>The <a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/2841\">DukeSpace repository hosts articles made available under this policy</a>, as well as <a href=\"https://dukespace.lib.duke.edu/dspace/\">other scholarly resources from Duke</a>. Duke faculty may <a href=\"https://scholarworks.duke.edu/elements/\">deposit their work to DukeSpace via the Elements system</a>, while building their <a href=\"https://scholars.duke.edu/\">Scholars@Duke</a> profile.</p><h3>Compact for Open Access Publishing Equity (COPE) Fund</h3><p><a href=\"https://en.wikipedia.org/wiki/Open-access_journal\">Open-access journals</a> make their articles available freely to anyone, without subscription costs, while providing the same peer review and other services common to all scholarly journals. To cover the costs of these services, some OA journals charge fees to authors instead, once their work has been selected for publication.</p><p><a href=\"https://today.duke.edu/2010/10/open_access.html\">In 2010</a>,&nbsp;Duke joined&nbsp;the&nbsp;<a href=\"https://www.oacompact.org/\">Compact for Open-Access Publishing Equity (COPE)</a>&nbsp;and established a fund to help Duke researchers cover author fees and to remove the financial barriers to publishing in OA journals.</p><p><a href=\"/research/openaccess/cope\">Learn whether you are eligible for COPE funds and request help.</a></p><h3>Electronic Theses &amp; Dissertations</h3><p>The&nbsp;<a href=\"https://gradschool.duke.edu/academics/theses-and-dissertations\">Duke University Graduate School requires all students to submit their theses and dissertations electronically</a>. All submissions&nbsp;are accessible in the&nbsp;<a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/1\">DukeSpace repository</a>&nbsp;under a&nbsp;<a href=\"https://creativecommons.org/licenses/by-nc-nd/3.0/us/\">Creative Commons license</a>. Authors have the option of placing&nbsp;a temporary embargo on their work.&nbsp;</p><ul><li><a href=\"https://gradschool.duke.edu/academics/theses-and-dissertations\">Learn how to submit your thesis or dissertation.&nbsp;</a></li><li><a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/1\">Read other theses and dissertations in DukeSpace</a>.</li></ul><h3>Open Access Journal Publishing</h3><p>If you're interested in creating a peer-reviewed online journal or in transitioning an existing one to an open access online version,&nbsp;<a href=\"openaccess/journals\">Duke University Libraries can help by providing a platform and technical support</a>. We currently&nbsp;<a href=\"openaccess/journals\">support several journals</a>&nbsp;using the&nbsp;<a href=\"https://pkp.sfu.ca/ojs\">Open Journal Systems</a>&nbsp;platform.</p><h2>Open Access at Duke Law</h2><p>In 1998, the <a href=\"https://law.duke.edu/\">Duke University School of Law</a>&nbsp;became the first in the country to make&nbsp;<a href=\"https://law.duke.edu/libtech/openaccess\">all the articles published in its law journals — including back issues — freely accessible online</a>. The&nbsp;<a href=\"https://scholarship.law.duke.edu/\">Duke Law Scholarship Repository</a>&nbsp;continues to provide free, full-text access to more than 3,000 scholarly articles written by Duke Law faculty or published in Duke Law journals.</p><h2>Open Access at Duke Medical Center</h2><p>The&nbsp;<a href=\"https://mclibrary.duke.edu/\">Duke Medical Center Library &amp; Archives</a>&nbsp;supports open access to health and medical information.</p><ul><li><a href=\"http://guides.mclibrary.duke.edu/openaccess\">Read the Medical Center Library's guide to open access</a></li><li><a href=\"http://guides.mclibrary.duke.edu/nihpublicaccess\">Follow a tutorial about the National Institutes of Health public access policy</a></li></ul><p>&nbsp;</p>"
   },
   "vid" : 18365,
   "uuid" : "f6a47b33-8fc2-4be8-93e2-834c60ab6b33",
   "langcode" : "und",
   "revisions" : [
      {
         "timestamp" : 1506613621,
         "uid" : 3,
         "promote" : 0,
         "comment" : 1,
         "sticky" : 0,
         "log" : "",
         "field_revision" : {
            "sticky" : 0,
            "revision_translation_affected" : 1,
            "default_langcode" : 1,
            "uid" : 1,
            "status" : 0,
            "langcode" : null,
            "created" : null,
            "changed" : null,
            "title" : "Open Access at Duke University"
         },
         "status" : 0,
         "node_revision__body" : {
            "langcode" : "und",
            "entity_id" : 357,
            "body_summary" : "",
            "body_value" : "<h2>What is Open Access?</h2><p><a href=\"https://en.wikipedia.org/wiki/Open_access\">Open access</a> (OA) is the practice of providing unrestricted access via the Internet to peer-reviewed scholarly research. It is most commonly applied to scholarly journal articles, but it is also increasingly being provided to theses, scholarly monographs and book chapters.</p><p>&nbsp;</p><p>At Duke, one of the signature themes of our mission is putting knowledge in the service of society. This means making the fruits of Duke research available as broadly as possible&nbsp;— not just to researchers at places like Duke that have subscription access to scholarly literature via their libraries but to anyone who might benefit from the scholarship being done here.</p><p>Duke University supports open access through a number of initiatives and encourages Duke scholars to work toward making their research openly accessible.</p><h2>Support for Open Access at Duke University</h2><h3>Open Access Policy for Faculty Scholarship</h3><p>In March 2010, the Duke University <a href=\"https://academiccouncil.duke.edu/\">Academic Council</a> <a href=\"https://today.duke.edu/2010/03/accessvote.html\">adopted an open access policy</a> that applies to all Duke faculty members and, unless individual authors choose to opt-out, provides Duke a license to make scholarly articles authored by Duke faculty freely available via a Duke University Libraries repository known as&nbsp;<a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/2841\" style=\"text-decoration: underline;\">DukeSpace</a>. The text of the policy is found in <a href=\"https://provost.duke.edu/wp-content/uploads/FHB_App_P.pdf\">Appendix P</a> of the <a href=\"https://provost.duke.edu/faculty-resources/faculty-handbook/\">Faculty Handbook</a>.&nbsp;If your publisher requests a formal letter waiving the faculty open access policy (i.e., asking you to opt out), please send an email to <a href=\"mailto:open-access@duke.edu\">open-access@duke.edu</a> that includes the name of the publisher and the citation of the article.</p><p>The <a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/2841\">DukeSpace repository hosts articles made available under this policy</a>, as well as <a href=\"https://dukespace.lib.duke.edu/dspace/\">other scholarly resources from Duke</a>. Duke faculty may <a href=\"https://scholarworks.duke.edu/elements/\">deposit their work to DukeSpace via the Elements system</a>, while building their <a href=\"https://scholars.duke.edu/\">Scholars@Duke</a> profile.</p><h3>Compact for Open Access Publishing Equity (COPE) Fund</h3><p><a href=\"https://en.wikipedia.org/wiki/Open-access_journal\">Open-access journals</a> make their articles available freely to anyone, without subscription costs, while providing the same peer review and other services common to all scholarly journals. To cover the costs of these services, some OA journals charge fees to authors instead, once their work has been selected for publication.</p><p><a href=\"https://today.duke.edu/2010/10/open_access.html\">In 2010</a>,&nbsp;Duke joined&nbsp;the&nbsp;<a href=\"https://www.oacompact.org/\">Compact for Open-Access Publishing Equity (COPE)</a>&nbsp;and established a fund to help Duke researchers cover author fees and to remove the financial barriers to publishing in OA journals.</p><p><a href=\"/research/openaccess/cope\">Learn whether you are eligible for COPE funds and request help.</a></p><h3>Electronic Theses &amp; Dissertations</h3><p>The&nbsp;<a href=\"https://gradschool.duke.edu/academics/theses-and-dissertations\">Duke University Graduate School requires all students to submit their theses and dissertations electronically</a>. All submissions&nbsp;are accessible in the&nbsp;<a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/1\">DukeSpace repository</a>&nbsp;under a&nbsp;<a href=\"https://creativecommons.org/licenses/by-nc-nd/3.0/us/\">Creative Commons license</a>. Authors have the option of placing&nbsp;a temporary embargo on their work.&nbsp;</p><ul><li><a href=\"https://gradschool.duke.edu/academics/theses-and-dissertations\">Learn how to submit your thesis or dissertation.&nbsp;</a></li><li><a href=\"https://dukespace.lib.duke.edu/dspace/handle/10161/1\">Read other theses and dissertations in DukeSpace</a>.</li></ul><h3>Open Access Journal Publishing</h3><p>If you're interested in creating a peer-reviewed online journal or in transitioning an existing one to an open access online version,&nbsp;<a href=\"openaccess/journals\">Duke University Libraries can help by providing a platform and technical support</a>. We currently&nbsp;<a href=\"openaccess/journals\">support several journals</a>&nbsp;using the&nbsp;<a href=\"https://pkp.sfu.ca/ojs\">Open Journal Systems</a>&nbsp;platform.</p><h2>Open Access at Duke Law</h2><p>In 1998, the <a href=\"https://law.duke.edu/\">Duke University School of Law</a>&nbsp;became the first in the country to make&nbsp;<a href=\"https://law.duke.edu/libtech/openaccess\">all the articles published in its law journals — including back issues — freely accessible online</a>. The&nbsp;<a href=\"https://scholarship.law.duke.edu/\">Duke Law Scholarship Repository</a>&nbsp;continues to provide free, full-text access to more than 3,000 scholarly articles written by Duke Law faculty or published in Duke Law journals.</p><h2>Open Access at Duke Medical Center</h2><p>The&nbsp;<a href=\"https://mclibrary.duke.edu/\">Duke Medical Center Library &amp; Archives</a>&nbsp;supports open access to health and medical information.</p><ul><li><a href=\"http://guides.mclibrary.duke.edu/openaccess\">Read the Medical Center Library's guide to open access</a></li><li><a href=\"http://guides.mclibrary.duke.edu/nihpublicaccess\">Follow a tutorial about the National Institutes of Health public access policy</a></li></ul><p>&nbsp;</p>",
            "revision_id" : 18365,
            "deleted" : 0,
            "body_format" : "full_html",
            "bundle" : "research_page",
            "delta" : 0
         },
         "node_revision__field_tags" : [],
         "vuuid" : "57b58d6f-603e-4371-847b-dbe56a513b4d",
         "title" : "Open Access at Duke University"
      }
   ]
}

