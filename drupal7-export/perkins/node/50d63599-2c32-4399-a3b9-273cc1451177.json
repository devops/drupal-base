{
   "asidebox" : {
      "block_delta" : null,
      "status" : "html",
      "bid" : null,
      "vid" : 28907,
      "body_title" : "",
      "nid" : 2455,
      "block_module" : null,
      "body_value" : "<h4>Who is this for?</h4><p>The intended audience for this policy includes faculty, administrators, and students of Duke University, as well as researchers external to Duke who are collaborating with Duke personnel and whose projects will make use of Duke University Libraries’ repository services.</p><h4>Related pages</h4><ul><li><a href=\"recommended-file-formats-digital-preservation\">Recommended File Formats for Digital Preservation</a></li><li><a href=\"digital-preservation-guide\">Digital Preservation Guide</a></li></ul>",
      "body_format" : "full_html"
   },
   "node_access" : null,
   "path_alias" : {
      "langcode" : "und",
      "status" : 0,
      "pid" : 4729,
      "alias" : "/using/policies/digital-preservation",
      "language" : "und"
   },
   "node_field_data" : {
      "created" : 1583876349,
      "status" : 1,
      "default_langcode" : 1,
      "sticky" : 0,
      "title" : "Digital Preservation Policy",
      "langcode" : "und",
      "uid" : 1,
      "revision_translation_affected" : 1,
      "type" : "page",
      "promote" : 0,
      "changed" : null
   },
   "field_tags" : [],
   "nid" : "2455",
   "langcode" : "und",
   "revisions" : [
      {
         "node_revision__field_tags" : [],
         "title" : "Digital Preservation Policy",
         "vuuid" : "f762338f-2b57-45c0-ba67-16955a379685",
         "node_revision__body" : {
            "body_value" : "<h2>\r\n    Purpose\r\n</h2>\r\n<p>\r\n    Duke University Libraries (DUL) and Duke University are committed to the long-term preservation and persistent access to\r\n    the University’s digital assets curated in the Duke Digital Repositories. Duke University Libraries are committed to the\r\n    stewardship of these materials into the future. This Digital Preservation Policy describes our overarching approach\r\n    to sustainable access for digital objects*. Detailed strategies for preservation activities are documented in the Preservation\r\n    Strategies below. Helpful guidance and general information about preservation strategies can be found on the <a href=\"digital-preservation-guide\" class=\"btn btn-primary btn-xs\">Digital Preservation Guide</a>.\r\n\r\n</p>\r\n</center>\r\n</div>\r\n\r\n<p>Preservation objectives are:\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        to provide sustainable, trustworthy environments in which digital assets are preserved and, when appropriate, made accessible\r\n        through persistent identifiers\r\n    </li>\r\n    <li>\r\n        to provide bit-level preservation to all deposited objects and associated metadata&nbsp;&nbsp;\r\n    </li>\r\n    <li>\r\n        to satisfy the requirements of funding agencies in managing, sharing and preserving research data\r\n    </li>\r\n    <li>\r\n        to verify and demonstrate authenticity of all deposited objects and metadata\r\n    </li>\r\n    <li>\r\n        to, whenever possible, maintain citability of accessible materials\r\n    </li>\r\n</ul>\r\n</p>\r\n\r\n<h2>\r\n    Scope\r\n</h2>\r\n<p>\r\n    This policy covers all materials selected for inclusion in the Duke Digital Repositories and individual repository collection\r\n    policies. This policy applies for the intended duration of stewardship for each collection. These policies are subject to\r\n    change by the Duke University Libraries and are not specific to systems or software implementation.\r\n</p>\r\n<p>\r\n    This policy does not address issues outside the scope of preservation such as collection development policies, retention\r\n    schedules, or selection criteria for higher levels of preservation service.\r\n</p>\r\n<p>\r\n    <img src=\"https://i.imgur.com/zALZ6mY.jpg\" alt=\"decorative image of preservation in action\" width=\"550\" style=\"vertical-align: middle;\"\r\n    class=\"image-center img-polaroid img-responsive\">\r\n</p>\r\n<h2>\r\n    Preservation strategies\r\n</h2>\r\n<h3>\r\n    Fixity checking and version control\r\n</h3>\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        Bit-level preservation will be provided to all digital objects in the repositories\r\n    </li>\r\n    <li>\r\n        Depositors may submit <a href=\"digital-preservation-explainer\">fixity</a> (or the assurance that the digital file hasn't changed) information along with objects to validate against during ingest. Fixity checks are performed\r\n        by the repository at regular intervals.\r\n    </li>\r\n    <li>\r\n        Objects that fail a fixity check will be restored from a backup copy\r\n    </li>\r\n</ul>\r\n<h3>\r\n    Multiple copies\r\n</h3>\r\n<p>\r\n    Duke University Libraries maintains at least three distinct replicas of data at all times. We contract with\r\n    the campus Office of Information Technology to support data replicas. The primary copy of the data is locally-stored at Duke\r\n    with a secondary, nearline copy also at Duke, and a tertiary copy on tape. Plans for an off-site, geographically distributed\r\n    fourth copy are under consideration.\r\n</p>\r\n<h3>\r\n    File formats and obsolescence management\r\n</h3>\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        We surveyed and evaluated file formats to create <a href=\"recommended-file-formats-digital-preservation\" class=\"btn btn-primary btn-xs\">a list of preferred file formats</a> in 2019\r\n    </li>\r\n    <li>\r\n        Prior to ingest into the repository, select file formats may be migrated to formats presenting less risk\r\n    </li>\r\n    <li>\r\n        Formats that are not preferred may still be included and receive bit-level preservation\r\n    </li>\r\n    <li>\r\n        Support for descriptive, administrative, technical, and preservation metadata to ensure continued understanding of the materials&nbsp;\r\n    </li>\r\n</ul>\r\n<h3>\r\n    Information Security\r\n</h3>\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        Authorization to read, write, move and/or delete materials and metadata will be controlled\r\n    </li>\r\n    <li>\r\n       We will maintain audit logs of read, write, edit, and delete actions taken and review as necessary\r\n    </li>\r\n</ul>\r\n<div class=\"striped-back\">\r\n    *Revised Policy updated on January 2, 2020. Acknowledging the rapid, ongoing changes in this technological environment, policies are subject to change.\r\n</div>",
            "revision_id" : 28907,
            "body_format" : "full_html_no_wysiwyg",
            "delta" : 0,
            "bundle" : "page",
            "deleted" : 0,
            "entity_id" : 2455,
            "body_summary" : "",
            "langcode" : "und"
         },
         "field_revision" : {
            "title" : "Digital Preservation Policy",
            "changed" : null,
            "created" : null,
            "uid" : 1,
            "langcode" : null,
            "status" : 1,
            "default_langcode" : 1,
            "revision_translation_affected" : 1,
            "sticky" : 0
         },
         "status" : 1,
         "log" : "",
         "comment" : 1,
         "sticky" : 0,
         "promote" : 0,
         "uid" : 2077,
         "timestamp" : 1587656586
      }
   ],
   "type" : "page",
   "field_image" : null,
   "uuid" : "50d63599-2c32-4399-a3b9-273cc1451177",
   "vid" : 28907,
   "node__body" : {
      "langcode" : "und",
      "body_summary" : "",
      "entity_id" : 2455,
      "deleted" : 0,
      "body_format" : "full_html_no_wysiwyg",
      "delta" : 0,
      "bundle" : "page",
      "body_value" : "<h2>\r\n    Purpose\r\n</h2>\r\n<p>\r\n    Duke University Libraries (DUL) and Duke University are committed to the long-term preservation and persistent access to\r\n    the University’s digital assets curated in the Duke Digital Repositories. Duke University Libraries are committed to the\r\n    stewardship of these materials into the future. This Digital Preservation Policy describes our overarching approach\r\n    to sustainable access for digital objects*. Detailed strategies for preservation activities are documented in the Preservation\r\n    Strategies below. Helpful guidance and general information about preservation strategies can be found on the <a href=\"digital-preservation-guide\" class=\"btn btn-primary btn-xs\">Digital Preservation Guide</a>.\r\n\r\n</p>\r\n</center>\r\n</div>\r\n\r\n<p>Preservation objectives are:\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        to provide sustainable, trustworthy environments in which digital assets are preserved and, when appropriate, made accessible\r\n        through persistent identifiers\r\n    </li>\r\n    <li>\r\n        to provide bit-level preservation to all deposited objects and associated metadata&nbsp;&nbsp;\r\n    </li>\r\n    <li>\r\n        to satisfy the requirements of funding agencies in managing, sharing and preserving research data\r\n    </li>\r\n    <li>\r\n        to verify and demonstrate authenticity of all deposited objects and metadata\r\n    </li>\r\n    <li>\r\n        to, whenever possible, maintain citability of accessible materials\r\n    </li>\r\n</ul>\r\n</p>\r\n\r\n<h2>\r\n    Scope\r\n</h2>\r\n<p>\r\n    This policy covers all materials selected for inclusion in the Duke Digital Repositories and individual repository collection\r\n    policies. This policy applies for the intended duration of stewardship for each collection. These policies are subject to\r\n    change by the Duke University Libraries and are not specific to systems or software implementation.\r\n</p>\r\n<p>\r\n    This policy does not address issues outside the scope of preservation such as collection development policies, retention\r\n    schedules, or selection criteria for higher levels of preservation service.\r\n</p>\r\n<p>\r\n    <img src=\"https://i.imgur.com/zALZ6mY.jpg\" alt=\"decorative image of preservation in action\" width=\"550\" style=\"vertical-align: middle;\"\r\n    class=\"image-center img-polaroid img-responsive\">\r\n</p>\r\n<h2>\r\n    Preservation strategies\r\n</h2>\r\n<h3>\r\n    Fixity checking and version control\r\n</h3>\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        Bit-level preservation will be provided to all digital objects in the repositories\r\n    </li>\r\n    <li>\r\n        Depositors may submit <a href=\"digital-preservation-explainer\">fixity</a> (or the assurance that the digital file hasn't changed) information along with objects to validate against during ingest. Fixity checks are performed\r\n        by the repository at regular intervals.\r\n    </li>\r\n    <li>\r\n        Objects that fail a fixity check will be restored from a backup copy\r\n    </li>\r\n</ul>\r\n<h3>\r\n    Multiple copies\r\n</h3>\r\n<p>\r\n    Duke University Libraries maintains at least three distinct replicas of data at all times. We contract with\r\n    the campus Office of Information Technology to support data replicas. The primary copy of the data is locally-stored at Duke\r\n    with a secondary, nearline copy also at Duke, and a tertiary copy on tape. Plans for an off-site, geographically distributed\r\n    fourth copy are under consideration.\r\n</p>\r\n<h3>\r\n    File formats and obsolescence management\r\n</h3>\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        We surveyed and evaluated file formats to create <a href=\"recommended-file-formats-digital-preservation\" class=\"btn btn-primary btn-xs\">a list of preferred file formats</a> in 2019\r\n    </li>\r\n    <li>\r\n        Prior to ingest into the repository, select file formats may be migrated to formats presenting less risk\r\n    </li>\r\n    <li>\r\n        Formats that are not preferred may still be included and receive bit-level preservation\r\n    </li>\r\n    <li>\r\n        Support for descriptive, administrative, technical, and preservation metadata to ensure continued understanding of the materials&nbsp;\r\n    </li>\r\n</ul>\r\n<h3>\r\n    Information Security\r\n</h3>\r\n<ul class=\"spacing-sm list-styled\">\r\n    <li>\r\n        Authorization to read, write, move and/or delete materials and metadata will be controlled\r\n    </li>\r\n    <li>\r\n       We will maintain audit logs of read, write, edit, and delete actions taken and review as necessary\r\n    </li>\r\n</ul>\r\n<div class=\"striped-back\">\r\n    *Revised Policy updated on January 2, 2020. Acknowledging the rapid, ongoing changes in this technological environment, policies are subject to change.\r\n</div>",
      "revision_id" : 28907
   }
}

