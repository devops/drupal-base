<?php

namespace Drupal\dul_ingest\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Symfony\Component\Yaml\Yaml;
use Drupal\node\Entity\Node;
use Drupal\block_content;
use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\Core\Language\LanguageInterface;
use Drupal\file\Entity\File;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\Core\Config\FileStorage;
use Drupal\taxonomy\Entity\Term;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DulIngestCommands extends DrushCommands {

  /**
   * Create DUL-specific content-types.
   *
   * @option option-name
   *   Description
   * @usage dul_ingest-createDULTypes foo
   *   Usage description
   *
   * @command dul_ingest:createDULTypes
   * @aliases cdt,dul-ctypes
   */
  public function createDULTypes($options = ['option-name' => 'default']) {

    // tip source:
    // https://www.drupal.org/forum/support/module-development-and-code-questions/2019-08-16/create-a-content-type
    $type = \Drupal\node\Entity\NodeType::create([
      'type' => 'rubenstein_page',
      'name' => 'Rubenstein Page',
    ]);
    $type->save();

    $type = \Drupal\node\Entity\NodeType::create([
      'type' => 'lilly_page',
      'name' => 'Lilly Page',
    ]);
    $type->save();

    $type = \Drupal\node\Entity\NodeType::create([
      'type' => 'music_page',
      'name' => 'Music Page',
    ]);
    $type->save();

    $type = \Drupal\node\Entity\NodeType::create([
      'type' => 'datavis_page',
      'name' => 'Data/Visualization Page',
    ]);
    $type->save();

    $type = \Drupal\node\Entity\NodeType::create([
      'type' => 'exhibits_page',
      'name' => 'Exhibits Page',
    ]);
    $type->save();

    foreach (['page', 'rubenstein_page', 'music_page', 'datavis_page', 'exhibits_page'] as $bundle) {
      \Drupal\field\Entity\FieldConfig::create([
        'entity_type'   => 'node',
        'bundle'        => $bundle,
        'field_name'    => 'field_image',
      ])->save();
    }

    // PRO TIPs from:
    // https://drupal.stackexchange.com/questions/289724/how-can-i-programmatically-create-a-block-type
    // https://api.drupal.org/api/drupal/core%21modules%21block_content%21block_content.module/function/block_content_add_body_field/8.5.x
    // -
    $asidebox_blocktype = BlockContentType::create([
      'id' => 'asidebox',
      'label' => 'Asidebox Block (legacy)',
      'description' => 'Asidebox Block for use in layout building.',
    ]);
    $asidebox_blocktype->save();
    $block_type_id = $asidebox_blocktype->id();
    block_content_add_body_field($block_type_id);

    // rebuild the cache
    $alias_manager = Drush::service('site.alias.manager');
    Drush::drush($alias_manager->getSelf(), 'cache-rebuild')->run();
    $this->logger()->success(dt("Cache rebuilt"));
  }

  /**
   * Command description here.
   *
   * @param $dataDir
   *   Directory where node data files (JSON) are located.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option content-type
   *   Content type to assign to ingested nodes (default: article)
   * @usage dul_ingest-commandName foo
   *   Usage description
   *
   * @command dul_ingest:nodeIngest
   * @aliases node-in
   */
  public function nodeIngest($dataDir, $options = ['content-type' => 'page']) {
    // Capture any failed ingest attempts for later data-correction
    // (hopefully not, however).
    $failed_ingests = [];

    // Remove "." and ".." from the array of file entries
    $scanned_directory = array_diff(scandir($dataDir), array('..', '.'));

    // process the individual JSON files, each representing
    // an exported node, generated from a separate process.
    // --
    // TODO add standardized export data structure
    foreach ($scanned_directory as $file) {
      try {
        // Decode as an associative array.
        // (json_decode returns 'stdClass' by default)
        $jsonData = file_get_contents($dataDir . '/' . $file);
        $node = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

        $node_data = $node['node_field_data'];

        $desired_content_type = $node['type'];

        // For the main instance (Perkins or "DUL"), enforce the content-type 
        // of 'article', unless the node's type is already 'page'.
        //
        // In other instances (Rubenstein, Lilly, etc),
        // force the new type to be the one provided by the --content-type option
        if ($options['content-type'] != 'page') {
          $desired_content_type = $options['content-type'];
        } else {
          $desired_content_type = 'page';
        }

        // THESE are the values to be saved with the 
        // soon-to-be created node.
        $edit = array(
          // set title
          'title' => $node['node_field_data']['title'],

          // unless explicitly set as 'page', force the node type = 'article'
          'type' => $desired_content_type,

          'body' => ['value' => $node['node__body']['body_value'], 'format' => $node['node__body']['body_format']],

          'sticky' => $node_data['sticky'],
          'promote' => $node_data['promote'],
          'uuid' => $node['uuid'],
        );
        $new_node = Node::create($edit);
        $new_node->save();
        $nid = $new_node->id();
        $this->logger()->success(dt('Created node for nid: ' . $nid));
        // END node creation

        // PATH ALIAS
        // create the path_alias entry for this node
        $path_alias = PathAlias::create([
          'path' => '/node/' . $nid,

          # NOTE: make sure we have the appropriate prefix for
          # this URL (e.g. rubenstein, music, lilly, datavis)
          # when it's not from the main 'perkins' instance.
          'alias' => $node['path_alias']['alias']
        ])->save();
        $this->logger()->success(dt('Added path alias: ') . $node['path_alias']['alias']);
        // END PATH ALIAS

        // ASIDEBOX
        // Add an asidebox_block entry when present
        if ($node['asidebox'] != null) {
          $block_info = $node['asidebox']['body_title'] == "" ? "[Asidebox]: " . $node['node_field_data']['title'] : $node['asidebox']['body_title'];
          $asidebox_block = BlockContent::create([
            'info' => $block_info,
            'type' =>'asidebox',
            'body' => ['value' => $node['asidebox']['body_value'], 'format' => $node['asidebox']['body_format']],
            'langcode' => 'en',
          ]);
          try {
            $asidebox_block->save();
          } catch (Exception $exc) {
            $this->logger()->error($exc->getMessage());
          }
          $this->logger()->success(dt("Added 'asidebox' content for node..."));
        }
        // END ASIDEBOX

        // ASSIGNED IMAGES (field_image)
        // -----------------------------
        // Does this node have an assigned image?
        //$this->logger()->notice(print_r($node, TRUE));
        if ($node['field_image'] != null) {

          $query = \Drupal::entityQuery('file')
            ->condition('uuid', $node['field_image']['file_uuid']);

          $results = $query->execute();
          $target_fid = array_pop($results);

          $new_node->set('field_image', $target_fid);
          $new_node->save();
          $this->logger()->success(dt('Added image to the node...'));
        }
        // END ASSIGNED IMAGES

      } catch(Exception $e) {
        $this->logger()->error("Error processing content: " . $node['node_field_data']['title]']);
        array_push($failed_ingests, [ 'nid' => $node['nid'], 'vid' => $node['vid']]);
        continue;
      }
    }

    // rebuild the cache
    $alias_manager = Drush::service('site.alias.manager');
    Drush::drush($alias_manager->getSelf(), 'cache-rebuild')->run();
    $this->logger()->success(dt("Cache rebuilt"));
  }

  /**
   * Ingest File Managed entries from batch of JSON files
   *
   * @param $dataDir
   *   Directory where file_managed JSON files are located
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option option-name
   *   Description
   * @usage dul_ingest-taxonomyIngest foo2
   *   Usage description
   *
   * @command dul_ingest:fileManagedIngest
   * @aliases file-in
   */
  public function fileManagedIngest($dataDir, $options = ['option-name' => 'default']) {
    $failed_ingests = [];

    // Remove "." and ".." from the array of file entries
    $scanned_directory = array_diff(scandir($dataDir), array('..', '.'));

    // process the individual JSON files, each representing
    // an exported node, generated from a separate process.
    // --
    // TODO add standardized export data structure
    foreach ($scanned_directory as $file) {
      try {
        // Decode as an associative array.
        // (json_decode returns 'stdClass' by default)
        $jsonData = file_get_contents($dataDir . '/' . $file);
        $file_managed = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

        $edit = array(
          'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,

          // preserve the original UUID
          'uuid' => $file_managed['uuid'],

          'filename' => $file_managed['filename'],
          'uri' => $file_managed['uri'],
          'filemine' => $file_managed['filemime'],
          'filesize' => $file_managed['filesize'],
          'status' => $file_managed['status']
        );

        $new_file = File::create($edit);
        $new_file->save();
        $fid = $new_file->id();
        $this->logger()->success(dt('Created managed file: ') . $file_managed['filename']);
        
      } catch(Exception $e) {
        array_push($failed_ingests, [ 'uuid' => $file_managed['uuid'], 'uri' => $file_managed['uri'] ]);
        continue;
      }
    }

    $query = \Drupal::entityQuery('file')
      ->condition('uuid', '11a69f73-9fe2-4777-a408-fe597ed25562')
      ->addTag('debug');
    $r = $query->execute();
    $this->logger->notice(print_r($r, TRUE));

    // rebuild the cache
    $alias_manager = Drush::service('site.alias.manager');
    Drush::drush($alias_manager->getSelf(), 'cache-rebuild')->run();
    $this->logger()->success(dt("Cache rebuilt"));

  }

 
  /**
   * Ingest Taxonomy Vocabulary entries from batch of JSON files
   *
   * @param $yamlDir
   *   Directory taxonomy-related JSON files are located
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option option-name
   *   Description
   * @usage dul_ingest-taxonomyIngest foo2
   *   Usage description
   *
   * @command dul_ingest:taxonomyIngest
   * @aliases tax-in
   */
  public function taxonomyIngest($dataDir, $options = ['option-name' => 'default']) {
    $config_path  = $yamlDir . '/vocabulary.yaml';

    //$vocab_yaml = Yaml::parse(file_get_contents($yamlDir . '/vocabulary.yaml'));

    $jsonData = file_get_contents($dataDir . 'vocabulary.json');
    $vocab_json = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

    try {
      foreach ($vocab_json['vocabulary'] as $vocab) {
        \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary')
          ->create([
            'vid' => $vocab['machine_name'],
            'weight' => $vocab['weight'],
            'description' => $vocab['description'],
            'name' => $vocab['name'],
          ])
          ->save();
      }
    } catch(\Drupal\Core\Entity\EntityStorageException $exc) {
      $this->logger()->warning(dt($exc->getMessage()));
    }

    $scanned_directory = array_diff(scandir($dataDir . "/terms"), array('..', '.'));
    foreach ($scanned_directory as $terms_file) {
      $this->logger()->notice(dt("Opening " . $dataDir . "/terms/" . $terms_file));
      $jsonData = file_get_contents($dataDir . '/terms/' . $terms_file);
      $t = json_decode($jsonData, TRUE, 512, JSON_THROW_ON_ERROR);

      // $t = Yaml::parse(file_get_contents($dataDir . '/terms/' . $terms_file));
      
      // create the term, making sure to retain the UUID from 
      // DUL's previous Drupal 7 instance.
      $new_term = Term::create([
        'name'        => $t['term']['name'],
        'vid'         => $t['term']['vocab_machine_name'],
        'description' => ['value' => $t['term']['description'], 'format' => $t['term']['format']],
        'weight'      => $t['term']['weight'],
        'uuid'        => $t['term']['uuid'],
      ]);
      $new_term->save();
      $this->logger()->notice(dt("Added term: " . $new_term->getName()));
    }

    $alias_manager = Drush::service('site.alias.manager');
    Drush::drush($alias_manager->getSelf(), 'cache-rebuild')->run();
    $this->logger()->success(dt("Cache rebuilt"));
  }

  /**
   * An example of the table output format.
   *
   * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @field-labels
   *   group: Group
   *   token: Token
   *   name: Name
   * @default-fields group,token,name
   *
   * @command dul_ingest:token
   * @aliases token
   *
   * @filter-default-field name
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function token($options = ['format' => 'table']) {

    // Process any assigned tags if present.
    //
    // When present, the tag can be identified by its
    // UUID which is preseved in the export process
    // (refer to Derrek's simple Perl script)

    # DEPRECATED -- not bringing over tags
    /*
    if (count($node['field_tags']) > 0) {
      // this will be the array of target term IDs (tid)
      // to add to the new_node
      $target_tids = [];
      foreach ($node['field_tags'] as $tag_data) {

        $query = \Drupal::entityQuery('taxonomy_term')
          ->condition('uuid', $tag_data['term_uuid']);

        $results = $query->execute();
        $target_tids = array_merge($target_tids, array_values($results));
      }
      $new_node->set('field_tags', $target_tids);
      $new_node->save();
      $this->logger()->success(dt('Added tags to the node...'));
    }
    */

    // BUSINESS LOGIC -- we are not creating a new revision
    // beyond the one created above when the node is saved.
    // --
    // THE CODE BELOW IS HERE ONLY AS A REFERENCE FOR THE QUESTION:
    // "How do we programmitically create a node revision?"
    // ********************************************
    // $new_node->setNewRevision();
    // $new_node->revision_log = 'Created revision for node ' . $nid;
    // $new_node->setRevisionCreationTime(REQUEST_TIME);
    // $new_node->save();
    // ********************************************
  }
}
