# Drupal 9 Base Image (OKD)
[[_TOC_]]

[Drupal](https://www.drupal.org/) is one of the most versatile open source content management systems on the market.  
  
## Read about Bitnami/Drupal
[Bitnami/Drupal](https://github.com/bitnami/bitnami-docker-drupal)  

## DUL Developer Prerequisites

- OpenShift CLI [see here](https://www.techrepublic.com/article/how-to-install-openshift-origin-on-ubuntu-18-04/).  The instructions should work for non-Ubuntu users, too.  

- Helm 3.1.0 [see here](https://helm.sh/docs/intro/install/)

## TL;DR (DUL-specific)
### Install Prerequisites
See above.

### Add Wrapper Script to PATH
```sh
# add /path/to/this/drupal-base/bin to your $PATH
# so you can use the 'restart-drupal9' wrapper script
$ PATH=$PATH:/path/to/drupal-base/bin
```

### Obtain 'oc login' URL
Visit the [OKD UI site](https://manage.cloud.duke.edu) and click the "Copy Login Command" link.  
  
With that, you're ready to use the command-line to do the following...

### CLI STUFF
```console
$ oc login https://manage.cloud.duke.edu:443 --token=<redacted-see-note>
$ cd /path/to/this/repo

# OPTIONAL: Assuming a recent commit
$ export COMMIT_REF=`git log -1 --pretty=%h`

# building image/pushing to registry
$ sudo docker build -t drupal-base
$ sudo docker login -u openshift -p $(oc whoami -t) registry.cloud.duke.edu
$ sudo docker tag drupal9-base registry.cloud.duke.edu/<YOUR-NAMESPACE>/drupal9-base
$ sudo docker push registry.cloud.duke.edu/<YOUR-NAMESPACE>/drupal9-base

# deploying to our current OKD platform
$ oc project <YOUR-NAMESPACE>

# call the wrapper script
$ restart-drupal9
```

## LIST OF CUSTOMIZATIONS/HACKS

* Added "uid" attribute to local copy of [duke-shibboleth](https://gitlab.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth/-/tree/main/helm-chart/duke-shibboleth/templates) "configmaps.yml" template.
