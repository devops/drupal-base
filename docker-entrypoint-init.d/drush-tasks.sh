#!/bin/bash

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

# Tell Drupal we're running behind a reverse proxy
# 
drupal_conf_set "\$settings['reverse_proxy']" "TRUE" yes
drupal_conf_set "\$settings['reverse_proxy_addresses']" "array(\$_SERVER['REMOTE_ADDR'])" yes
drupal_conf_set "\$settings['reverse_proxy_trusted_headers']" "\\\Symfony\\\Component\\\HttpFoundation\\\Request::HEADER_X_FORWARDED_ALL" yes

cd /opt/bitnami/drupal

info "Enabling DUL-specific components..."

drush theme:enable radix -y

drush theme:enable drupal9_dulcet -y
drush config-set system.theme default drupal9_dulcet -y

# Load all of the nodes/content types/files from our 
# migrated Drupal7 data
drush dul-ctypes
drush file-in $DUL_DRUPAL7_EXPORT_DIR/perkins/file_managed
drush node-in $DUL_DRUPAL7_EXPORT_DIR/perkins/node

drush file-in $DUL_DRUPAL7_EXPORT_DIR/rubenstein/file_managed
drush node-in $DUL_DRUPAL7_EXPORT_DIR/rubenstein/node

drush file-in $DUL_DRUPAL7_EXPORT_DIR/datagis/file_managed
drush node-in $DUL_DRUPAL7_EXPORT_DIR/datagis/node

drush file-in $DUL_DRUPAL7_EXPORT_DIR/east/file_managed
drush node-in $DUL_DRUPAL7_EXPORT_DIR/east/node

# finally rebuild the Drupal cache
info "Rebuilding the Drupal cache..."
drush cr

info "Done enabling DUL-specific components!"
