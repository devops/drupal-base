## Global Docker image parameters
## Please, note that this will override the image parameters, including dependencies, configured to use the global value
## Current available global Docker image parameters: imageRegistry and imagePullSecrets
##
# global:
#   imageRegistry: myRegistryName
#   imagePullSecrets:
#     - myRegistryKeySecretName
#   storageClass: myStorageClass

## Bitnami Drupal image version
## ref: https://hub.docker.com/r/bitnami/drupal/tags/
##
shibboleth: {}
shibImage:
  pullPolicy: Always
  registry: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth
  protectedServiceApp: ""
  name: duke-shibboleth
  version: centos7

image:
  registry: docker-registry.default.svc:5000
  repository: dul-sandbox/drupal9-modshib
  tag: latest
  # registry: docker.io
  # repository: bitnami/drupal
  # tag: 9.1.4-debian-10-r0
  #---
  ## Specify a imagePullPolicy
  ## Defaults to 'Always' if image tag is 'latest', else set to 'IfNotPresent'
  ## ref: http://kubernetes.io/docs/user-guide/images/#pre-pulling-images
  ##
  pullPolicy: Always
  ## Optionally specify an array of imagePullSecrets.
  ## Secrets must be manually created in the namespace.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  ##
  pullSecrets:
  #   - myRegistryKeySecretName
  ## Set to true if you would like to see extra information on logs
  ## ref: https://github.com/bitnami/minideb-extras/#turn-on-bash-debugging
  ##
  debug: true

## Force target Kubernetes version (using Helm capabilites if not set)
##
kubeVersion:

## String to partially override drupal.fullname template (will maintain the release name)
##
nameOverride:

## Deployment pod host aliases
## https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/
##
hostAliases:
  # Necessary for apache-exporter to work
  - ip: "127.0.0.1"
    hostnames:
      - "status.localhost"

## String to fully override drupal.fullname template
##
fullnameOverride:

## Number of replicas (requires ReadWriteMany PVC support)
##
replicaCount: 1

## Installation Profile
## ref: https://github.com/bitnami/bitnami-docker-drupal#configuration
##
drupalProfile: standard

## Skip Drupal installation wizard. Useful for migrations and restoring from SQL dump
## ref: https://github.com/bitnami/bitnami-docker-drupal#configuration
##
drupalSkipInstall: false

## User of the application
## ref: https://github.com/bitnami/bitnami-docker-drupal#configuration
##
drupalUsername: user

## Application password
## Defaults to a random 10-character alphanumeric string if not set
## ref: https://github.com/bitnami/bitnami-docker-drupal#configuration
##
drupalPassword: ""

## Admin email
## ref: https://github.com/bitnami/bitnami-docker-drupal#configuration
##
drupalEmail: user@example.com

## Set to `yes` to allow the container to be started with blank passwords
## ref: https://github.com/bitnami/bitnami-docker-drupal#environment-variables
##
allowEmptyPassword: true

## Container command (using container default if not set)
##
command:
## Container args (using container default if not set)
##
args:

## Common annotations to add to all Drupal resources (sub-charts are not considered). Evaluated as a template
##
commonAnnotations: {}

## Common labels to add to all Drupal resources (sub-charts are not considered). Evaluated as a template
##
commonLabels: {}

## Update strategy - only really applicable for deployments with RWO PVs attached
## If replicas = 1, an update can get "stuck", as the previous pod remains attached to the
## PV, and the "incoming" pod can never start. Changing the strategy to "Recreate" will
## terminate the single previous pod, so that the new, incoming pod can attach to the PV
##
updateStrategy:
  type: RollingUpdate

## An array to add extra env vars
## For example:
##
extraEnvVars: []
#  - name: BEARER_AUTH
#    value: true

## ConfigMap with extra environment variables
##
extraEnvVarsCM:

## Secret with extra environment variables
##
extraEnvVarsSecret:

## Extra volumes to add to the deployment
##
extraVolumes:
- name: socket-shared
  emptyDir: {}
- name: apconf
  configMap:
    name: apconf
    defaultMode: 420
    items:
    - key: application-vhost.conf
      path: application-vhost.conf
    - key: servername.conf
      path: servername.conf
    - key: httpd.conf
      path: httpd.conf
- name: shibconf
  configMap:
    name: shibconf
    defaultMode: 420
    items:
    - key: shibboleth2.xml
      path: shibboleth2.xml
    - key: attribute-map.xml
      path: attribute-map.xml
- name: drupal-sandbox-tls
  secret:
    secretName: drupal-sandbox.lib-tls
    defaultMode: 420

## Extra volume mounts to add to the container
##
extraVolumeMounts:
- name: socket-shared
  mountPath: /var/run/shibboleth
- name: shibconf
  mountPath: /etc/shibboleth/shibboleth2.xml
  readOnly: true
  subPath: shibboleth2.xml
- name: apconf
  mountPath: /opt/bitnami/apache/conf/httpd.conf
  subPath: httpd.conf
- name: apconf
  mountPath: /opt/bitnami/apache/conf/vhosts/application-vhost.conf
  subPath: application-vhost.conf
- name: apconf
  mountPath: /opt/bitnami/apache/conf/bitnami/servername.conf
  subPath: servername.conf

## Extra init containers to add to the deployment
##
initContainers: []

## Extra sidecar containers to add to the deployment
##
sidecars:
- name: drupal9-shibd
  image: gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth/duke-shibboleth:centos7
  imagePullPolicy: Always
  # the "extraVolumeMounts" are not shared with sidecar containers, 
  # so we need to specify them here.
  volumeMounts:
  - name: socket-shared
    mountPath: /var/run/shibboleth
  - name: shibconf
    mountPath: /etc/shibboleth/shibboleth2.xml
    readOnly: true
    subPath: shibboleth2.xml
  - name: shibconf
    mountPath: /etc/shibboleth/attribute-map.xml
    readOnly: true
    subPath: attribute-map.xml
  - name: drupal-sandbox-tls
    mountPath: /etc/shibboleth/tls.key
    readOnly: true
    subPath: tls.key
  - name: drupal-sandbox-tls
    mountPath: /etc/shibboleth/tls.crt
    readOnly: true
    subPath: tls.crt

## Tolerations for pod assignment. Evaluated as a template.
## Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
##
tolerations: []

## Use existing secret for the application password
##
existingSecret:

##
## External database configuration
##
externalDatabase:
  ## Database host
  ##
  host: drupal9-mariadb

  ## Database host
  ##
  port: 3306

  ## Database user
  ##
  user: bn_drupal

  ## Database password
  ##
  password: "bn_drupal"

  ## Database name
  ##
  database: bitnami_drupal

## SMTP mail delivery configuration
## ref: https://github.com/bitnami/bitnami-docker-drupal/#smtp-configuration
##
smtpHost:
smtpPort:
smtpUser:
smtpPassword:
smtpProtocol:

##
## MariaDB chart configuration
##
## https://github.com/bitnami/charts/blob/master/bitnami/mariadb/values.yaml
##
mariadb:
  ## Whether to deploy a mariadb server to satisfy the applications database requirements. To use an external database set this to false and configure the externalDatabase parameters
  ##
  enabled: true

  ## MariaDB architecture. Allowed values: standalone or replication
  ##
  architecture: standalone

  ## MariaDB Authentication parameters
  ##
  auth:
    ## MariaDB root password
    ## ref: https://github.com/bitnami/bitnami-docker-mariadb#setting-the-root-password-on-first-run
    ##
    rootPassword: ""
    ## MariaDB custom user and database
    ## ref: https://github.com/bitnami/bitnami-docker-mariadb/blob/master/README.md#creating-a-database-on-first-run
    ## ref: https://github.com/bitnami/bitnami-docker-mariadb/blob/master/README.md#creating-a-database-user-on-first-run
    ##
    database: bitnami_drupal
    username: bn_drupal
    password: ""

  primary:
    image:
      debug: true
    ## Disable securityContext
    podSecurityContext:
      enabled: false
    containerSecurityContext:
      enabled: false
    ## Enable persistence using Persistent Volume Claims
    ## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
    ##
    persistence:
      enabled: true
      ## mariadb data Persistent Volume Storage Class
      ## If defined, storageClassName: <storageClass>
      ## If set to "-", storageClassName: "", which disables dynamic provisioning
      ## If undefined (the default) or set to null, no storageClassName spec is
      ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
      ##   GKE, AWS & OpenStack)
      ##
      storageClass:
      accessModes:
        - ReadWriteOnce
      size: 8Gi
      ## Set path in case you want to use local host path volumes (not recommended in production)
      ##
      hostPath:
      ## Use an existing PVC
      ##
      existingClaim: 

## Container ports
##
containerPorts:
  http: 8080
  https: 8443

## Kubernetes configuration
## For minikube, set this to NodePort, elsewhere use LoadBalancer
##
service:
  type: LoadBalancer
  # HTTP Port
  port: 80
  # HTTPS Port
  httpsPort: 443
  ## clusterIP: ""
  ## Control hosts connecting to "LoadBalancer" only
  ## loadBalancerSourceRanges:
  ##   - 0.0.0.0/0
  ## loadBalancerIP for the Drupal Service (optional, cloud specific)
  ## ref: http://kubernetes.io/docs/user-guide/services/#type-loadbalancer
  ## loadBalancerIP:
  ##
  ## nodePorts:
  ##   http: <to set explicitly, choose port between 30000-32767>
  ##   https: <to set explicitly, choose port between 30000-32767>
  ##
  nodePorts:
    http: ""
    https: ""
  ## Enable client source IP preservation
  ## ref http://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip
  ##
  externalTrafficPolicy: Cluster

## Configure the ingress resource that allows you to access the
## Drupal installation. Set up the URL
## ref: http://kubernetes.io/docs/user-guide/ingress/
##
ingress:
  ## Set to true to enable ingress record generation
  ##
  enabled: true

  ## Set this to true in order to add the corresponding annotations for cert-manager
  ##
  certManager: false

  ## Ingress Path type
  ##
  pathType: ImplementationSpecific

  ## Override API Version (automatically detected if not set)
  ##
  apiVersion:

  ## When the ingress is enabled, a host pointing to this will be created
  ##
  ##hostname: drupal.local
  hostname: drupal-sandbox.lib.duke.edu

  ## The Path to Drupal. You may need to set this to '/*' in order to use this
  ## with ALB ingress controllers.
  ##
  path: /

  ## Ingress annotations done as key:value pairs
  ## For a full list of possible ingress annotations, please see
  ## ref: https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/annotations.md
  ##
  ## If certManager is set to true, annotation kubernetes.io/tls-acme: "true" will automatically be set
  ##
  annotations: {}

  ## Enable TLS configuration for the hostname defined at ingress.hostname parameter
  ## TLS certificates will be retrieved from a TLS secret with name: {{- printf "%s-tls" .Values.ingress.hostname }}
  ## You can use the ingress.secrets parameter to create this TLS secret or relay on cert-manager to create it
  ##
  tls: true

  ## The list of additional hostnames to be covered with this ingress record.
  ## Most likely the hostname above will be enough, but in the event more hosts are needed, this is an array
  ## extraHosts:
  ## - name: drupal.local
  ##   path: /
  ##

  ## Any additional arbitrary paths that may need to be added to the ingress under the main host.
  ## For example: The ALB ingress controller requires a special rule for handling SSL redirection.
  ## extraPaths:
  ## - path: /*
  ##   backend:
  ##     serviceName: ssl-redirect
  ##     servicePort: use-annotation
  ##

  ## The tls configuration for additional hostnames to be covered with this ingress record.
  ## see: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls
  ## extraTls:
  ## - hosts:
  ##     - drupal.local
  ##   secretName: drupal.local-tls
  ##

  ## If you're providing your own certificates, please use this to add the certificates as secrets
  ## key and certificate should start with -----BEGIN CERTIFICATE----- or
  ## -----BEGIN RSA PRIVATE KEY-----
  ##
  ## name should line up with a tlsSecret set further up
  ## If you're using cert-manager, this is unneeded, as it will create the secret for you if it is not set
  ##
  ## It is also possible to create and manage the certificates outside of this helm chart
  ## Please see README.md for more information
  ##
  secrets:
  - name: drupal9
    certificate: |
      -----BEGIN CERTIFICATE-----
      MIIHXzCCBkegAwIBAgIRAPgzoSGcOcfovk6Vn3bP7agwDQYJKoZIhvcNAQELBQAw
      djELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAk1JMRIwEAYDVQQHEwlBbm4gQXJib3Ix
      EjAQBgNVBAoTCUludGVybmV0MjERMA8GA1UECxMISW5Db21tb24xHzAdBgNVBAMT
      FkluQ29tbW9uIFJTQSBTZXJ2ZXIgQ0EwHhcNMjEwNDAxMDAwMDAwWhcNMjIwNTAy
      MjM1OTU5WjCBhTELMAkGA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5h
      MQ8wDQYDVQQHEwZEdXJoYW0xGDAWBgNVBAoTD0R1a2UgVW5pdmVyc2l0eTEMMAoG
      A1UECxMDT0lUMSQwIgYDVQQDExtkcnVwYWwtc2FuZGJveC5saWIuZHVrZS5lZHUw
      ggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDrW/5nl1Xot6MsqFiVuQHS
      4S/KQ4piKS63b3O4TOUNn/KZwNKUa1LL2kJHmMre1EdtZ//imuegRPBKjx/lIU83
      q1v0ymN8FpP6dfpQiO//M6hb0NGgru5blGCVkQZmXKTvqZXp7gCzxQkaeGKsmF55
      hisH8kjffmu2H9qnA2HxErHj/B8j9f22sZtXWViTnXQSqhSm5EgGkHzUfi1bvuGh
      OAyzxzrlCvM8l9nWZzrg5/k5HnRrjLOeNagXVCNoLA7Mnq6Qe6i9EjAFIV0JZroh
      WJASiVO6hQJ6R9pRoPaq3+wOrcMdKrw7MksnkB1ANaoWpJTlMT6LIsFm6LnPIPJr
      FQwnPaGPFsLAal3ZWI1wt9minhttBH6E1Fwn+JEWIz0YYbtjjELSnnkHZ1F50VuL
      yHgpoelXn8O1WRwGwMSteMcGSpNfXj3VdVSCB3zT+70jivGavg/kDmxvhFStARgc
      94MABztgNNdnyQXMvwV4XNPBdm5yOK2c+6inUtg0WHnZ4TESa2EVnSXOQpATsZb8
      jujrayJHWc4SSciBdewZzerAy7VGIG8UCsz8DaV/G90tcClWxKdryLNXZGySIW+h
      Hed8hgp1N6wTrCvXo5Fic/QVQi/Fn7cHvFlHXuKEPRg6fo8UXKYEFTFKchlaxV27
      Funz5erwtfR7vFwSZ9ct3QIDAQABo4IC1jCCAtIwHwYDVR0jBBgwFoAUHgWjd49s
      luJbh0umtIascQAM5zgwHQYDVR0OBBYEFDqBMWzEPAU2dnaPxyWjFwPa7nYDMA4G
      A1UdDwEB/wQEAwIFoDAMBgNVHRMBAf8EAjAAMB0GA1UdJQQWMBQGCCsGAQUFBwMB
      BggrBgEFBQcDAjBnBgNVHSAEYDBeMFIGDCsGAQQBriMBBAMBATBCMEAGCCsGAQUF
      BwIBFjRodHRwczovL3d3dy5pbmNvbW1vbi5vcmcvY2VydC9yZXBvc2l0b3J5L2Nw
      c19zc2wucGRmMAgGBmeBDAECAjBEBgNVHR8EPTA7MDmgN6A1hjNodHRwOi8vY3Js
      LmluY29tbW9uLXJzYS5vcmcvSW5Db21tb25SU0FTZXJ2ZXJDQS5jcmwwdQYIKwYB
      BQUHAQEEaTBnMD4GCCsGAQUFBzAChjJodHRwOi8vY3J0LnVzZXJ0cnVzdC5jb20v
      SW5Db21tb25SU0FTZXJ2ZXJDQV8yLmNydDAlBggrBgEFBQcwAYYZaHR0cDovL29j
      c3AudXNlcnRydXN0LmNvbTAmBgNVHREEHzAdghtkcnVwYWwtc2FuZGJveC5saWIu
      ZHVrZS5lZHUwggEDBgorBgEEAdZ5AgQCBIH0BIHxAO8AdQBGpVXrdfqRIDC1oolp
      9PN9ESxBdL79SbiFq/L8cP5tRwAAAXiOow0FAAAEAwBGMEQCIEUX8fd16z++SyEU
      cfqp8hil5LNDbPaWujWs8J5EtAbSAiAT5Z07kGtyS1ghNRDLiqcVFWVvdZ7UZg1h
      ObAaDljAIQB2AN+lXqtogk8fbK3uuF9OPlrqzaISpGpejjsSwCBEXCpzAAABeI6j
      DJYAAAQDAEcwRQIhAMC/IZ+xBS4qWzC8qkmaNmpTDjifH9Ccbaf+N7BNdGIiAiBG
      DB410QmCXQobgPASwIjKhQPQVzDJ0CBuvHTbJ4vOgTANBgkqhkiG9w0BAQsFAAOC
      AQEAcZY6xzSNFWEQKpfi7CFFT/VVK1jfXcamIcKb+QVfjYK/a4bNU3tANuKV3EMa
      ONbm0MInufSIFiPy3xhkKUq8TcqZ//q2ZRMpeku4zct6I817JPno04yU0+RF3zWx
      QcDtt8lM7T24T5C3jv+RLhC/5V7DL6FJ9AXfTCo2DGSe6/Tq9eUsKZLsb5pxCD9L
      Lkvgq85DvmE8R9q8D5g8v3aso4CK1sWMg4w5brZS4Wn0j8W7aiCsyEGUXZ2f6OOY
      QrBcZuQEF2V5aQ3UtKi8owLdiFlVOtB9m5plvYE+mOF8SksHm+F6W0eVR3rpKqWE
      iBUW8N7ZDR7NStL2i7tqax0HQw==
      -----END CERTIFICATE-----
    key: |
      -----BEGIN PRIVATE KEY-----
      MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQDrW/5nl1Xot6Ms
      qFiVuQHS4S/KQ4piKS63b3O4TOUNn/KZwNKUa1LL2kJHmMre1EdtZ//imuegRPBK
      jx/lIU83q1v0ymN8FpP6dfpQiO//M6hb0NGgru5blGCVkQZmXKTvqZXp7gCzxQka
      eGKsmF55hisH8kjffmu2H9qnA2HxErHj/B8j9f22sZtXWViTnXQSqhSm5EgGkHzU
      fi1bvuGhOAyzxzrlCvM8l9nWZzrg5/k5HnRrjLOeNagXVCNoLA7Mnq6Qe6i9EjAF
      IV0JZrohWJASiVO6hQJ6R9pRoPaq3+wOrcMdKrw7MksnkB1ANaoWpJTlMT6LIsFm
      6LnPIPJrFQwnPaGPFsLAal3ZWI1wt9minhttBH6E1Fwn+JEWIz0YYbtjjELSnnkH
      Z1F50VuLyHgpoelXn8O1WRwGwMSteMcGSpNfXj3VdVSCB3zT+70jivGavg/kDmxv
      hFStARgc94MABztgNNdnyQXMvwV4XNPBdm5yOK2c+6inUtg0WHnZ4TESa2EVnSXO
      QpATsZb8jujrayJHWc4SSciBdewZzerAy7VGIG8UCsz8DaV/G90tcClWxKdryLNX
      ZGySIW+hHed8hgp1N6wTrCvXo5Fic/QVQi/Fn7cHvFlHXuKEPRg6fo8UXKYEFTFK
      chlaxV27Funz5erwtfR7vFwSZ9ct3QIDAQABAoICAHAPqR3hrIHVjdfhf+wp3nht
      x7diWS1LtnaYllyWIClQks4qRRlt0EpHbW9xjIZ0RF4POoLGhqqEqHSXmPQb3AAB
      kxMGmj+m8GY8biaSTNMR4YN19ZmwMMVh7GrKVmW+jrmd4Uc0jxkJI8B6uWC8W585
      BpwN/CyNoPNiT1VeAvApmpGWpa0VnuuxvOjmHYqLWYzZdmPQJYsamJUETsx0IjIE
      4ednYN9VS2L6Wjn/hMU5LizIF15t68fUfGg/I2yiLZm3pB4fAsSjOEH5FLc5Nj5S
      UKbCxB723asjMwvSvB+35klG6rOkeWMmGyqHL6Y25vlI+6v0una84aECBhZc9Y6k
      89NpnvQ2ju0x51GdaXkL1cfo38sKrLO39faNbC42x0o4GzCFvmXsmrZOa0TRe3W5
      sGHwumJGAP2al7cEdH7cJ20pLvTyo9nTWuyE9OsJHumaGwu9v/mzz8mSEn3knyoM
      oGuUh3oPXYju3wpwlU83bKud1uExTiTU+dB+PHaEk87LGXaFlK4vptGZ1LrdoPmJ
      MLO/Wqtd22kbmfLRWr+R1f8pj161s5+DGzVUy52jMyrXxstazC8HdbIUQtcyOHst
      0HPeq2pgFEaUeMr5e2+VpchklQX7ZXs+UwAJ5aIOm0q4eo3h8p1isTjFZ1X32kZf
      F4LMR78q8XZlEXqyez1xAoIBAQD7Kp32cymP44tdTq5wP2q9b/ZbjMEX/4uSOo+F
      Q0IlI4gbbs35pJAFE2wP8UxUJuAl7REvleQamAsZzqhlMty3vzjmTpvBW3UZcXx2
      8QerZ/rNg9Tk3099Q6LHyPWTcNRr4Ieo/tFunATmCfPkakywST9Qq5XTir4l/GFe
      zlyjGhnElaczdCVPIlXuTz2lpTWaACYP8aakYVCoNe8jHY4bcc+R9eB3gY4dsBRl
      CnnrWBwlO4m1WdGoznOqP7Mr70fiKpjnI8ItZSFa/fr0qP9Kv7JeK+JNUKGTSkoG
      D1xrukOsleb4nxk93826+WtR6EeOO0N2EXbf4mATZSMP2jiDAoIBAQDv44CR4nHS
      D6VoQGVhTlTTNPjU2tbC/EeNdrNMl1bwLGRWqbY3oKAqq2IZs3Bc0FMes/RwH+ev
      zhN+GhBA5NioTREWMpll4KqHwYzVnNRvff48B17uEDH0rdNiCbLXYQBzIigf3ElH
      Zni9LfrKAM3pao2c1MD1iIz3F07YmJ7Df6+Uz6YqFWcyC7q+deT1bws6yiVHHo51
      YJ9x1SLRwqAz2rGhlyl/sP/7ATRRRgn1JgPH8hMC1jOLMo2Z6MgTyyAdOMHC8HhV
      /tPKs/ygrbP6h1REnWGxq43epUTYqdcWd5UxjomqgypUdVO0yz3bFzeqb1aWa61V
      82GJuXjPwHIfAoIBAQDrY2lfS1asT2iRLeg1snlqyL9t1WAyCYDYrqFLF1mqTL6G
      VzYavKts1mL0HgSW6UgwwxjUK/n3In9k1FTlYuGQI1pEHW/Dqt731W/8WLADaBOY
      //BJ/y8UCotICYJ9h8TMxUTekNHeg8HjhXhZnXX50OvRwr1L4D+MghcPLv+KrC9y
      GX71reg5XciC6yivUUIG7hS95I90hu98de+ETXKdLfCGrpSL65MxJ5A1kbfpZE4c
      ZswMdf0T40hrSf+1MS+BOsLJ/K1CtgCosla7bJa1iyqzLxWLuAF1dK4jN8o5+/ro
      iCRK5XBWmKTKkm6N+M728Xur73olIJdCFgriFHXZAoIBAG9vk44xTwZx1jizkWS+
      apEijmTys8koQGTquyfdNbmkJYhMliUJLN4ldxDRVmUHMVWV6vsbFxmH9W4i1WIP
      7MVh54oUP0YgM5eUxcOFcvnH4Y4pLmQQieRLy2JDdxyuJvNU7qa+Qhw08emPrmyg
      Kwc/REGSY4CryjAOV8YuP7I5YmsSevIZtzJ8WGeZhJ6u0tygguiivTKxi9/pTp96
      dckg0ltqSRDUPFVxncgTbRTAcAk13rb2okMLXNQeWrM3LNfmQbw40O3BzsjfC3/o
      sS60JuUo5vUN6H2A8JZlb1E0g83dlP3sqIFLb3MQ5s8b3fQCfIqrwn2m7ax4yDJa
      B6kCggEAc56LLv/dPLIZHCWGEMQkuAw4YkYYQTzNQ5BdiEmXUbmLBqNvXFOBVbRn
      fqDDqrFL47Ox7+oRvKVL9DIKr0fwrX3C3ukXm2+wcGxW0O+khNWh0A8l7dnKhd/w
      8GI1qneds76QwtKG2HIMr5D9td54VTS35gYRwsVdtpqVQf3npBR9hT8nZyaBV6IU
      HP4xA16nu0ODiYUxxzTyuyhtn867HJlf0gQulQiXEylUAyP2pJWo3KyXNTHKZTD1
      ZbYUvzgdg/qpki+hnTxwgzI7nd7OvnPlBVFFw5Se2j+EMnoRUF6bEQGbi2ZsW4yt
      NeH+xqMqu9i2PdAELDZpDjTWhSsxkA==
      -----END PRIVATE KEY-----

  ## - name: drupal.local-tls
  ##   key:
  ##   certificate:
  ##

## Control where client requests go, to the same pod or round-robin
## Values: ClientIP or None
## ref: https://kubernetes.io/docs/user-guide/services/
##
sessionAffinity: "None"

## Enable persistence using Persistent Volume Claims
## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
##
persistence:
  enabled: true
  ## Drupal Data Persistent Volume Storage Class
  ## If defined, storageClassName: <storageClass>
  ## If set to "-", storageClassName: "", which disables dynamic provisioning
  ## If undefined (the default) or set to null, no storageClassName spec is
  ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
  ##   GKE, AWS & OpenStack)
  ##
  # storageClass: "-"

  ## A manually managed Persistent Volume and Claim
  ## Requires persistence.enabled: true
  ## If defined, PVC must be created manually before volume will be bound
  ##
  accessMode: ReadWriteOnce
  size: 8Gi

  ## A manually managed Persistent Volume Claim
  ## Requires persistence.enabled: true
  ## If defined, PVC must be created manually before volume will be bound
  ##
  # existingClaim:

  ## If defined, the drupal-data volume will mount to the specified hostPath.
  ## Requires persistence.enabled: true
  ## Requires persistence.existingClaim: nil|false
  ## Default: nil.
  ##
  hostPath:

## Pod affinity preset
## ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity
## Allowed values: soft, hard
##
podAffinityPreset: ""

## Pod anti-affinity preset
## Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity
## Allowed values: soft, hard
##
podAntiAffinityPreset: soft

## Node affinity preset
## Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity
## Allowed values: soft, hard
##
nodeAffinityPreset:
  ## Node affinity type
  ## Allowed values: soft, hard
  ##
  type: ""
  ## Node label key to match
  ## E.g.
  ## key: "kubernetes.io/e2e-az-name"
  ##
  key: ""
  ## Node label values to match
  ## E.g.
  ## values:
  ##   - e2e-az1
  ##   - e2e-az2
  ##
  values: []

## Affinity for pod assignment
## Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
## Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set
##
affinity: {}

## Node labels for pod assignment. Evaluated as a template.
## ref: https://kubernetes.io/docs/user-guide/node-selection/
##
nodeSelector: {}

## Configure resource requests and limits
## ref: http://kubernetes.io/docs/user-guide/compute-resources/
##
resources:
  requests:
    memory: 512Mi
    cpu: 300m

## Init containers parameters:
## volumePermissions: Change the owner and group of the persistent volume mountpoint to runAsUser:fsGroup values from the securityContext section.
##
volumePermissions:
  enabled: false
  image:
    registry: docker.io
    repository: bitnami/minideb
    tag: buster
    pullPolicy: Always
    ## Optionally specify an array of imagePullSecrets.
    ## Secrets must be manually created in the namespace.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    ##
    pullSecrets: []
    ##   - myRegistryKeySecretName
  ## Init containers' resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ##
  resources:
    ## We usually recommend not to specify default resources and to leave this as a conscious
    ## choice for the user. This also increases chances charts run on environments with little
    ## resources, such as Minikube. If you do want to specify resources, uncomment the following
    ## lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    ##
    limits: {}
    ##   cpu: 100m
    ##   memory: 128Mi
    ##
    requests: {}
    ##   cpu: 100m
    ##   memory: 128Mi
    ##

## Configure Pods Security Context
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod
##
podSecurityContext:
  enabled: false
  fsGroup: 1001

## Configure Container Security Context (only main container)
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container
##
containerSecurityContext:
  enabled: false
  runAsUser: 1001

## Configure extra options for liveness and readiness probes
## Drupal core exposes /user/login to unauthenticated requests, making it a good
## default liveness and readiness path. However, that may not always be the
## case. For example, if the image value is overridden to an image containing a
## module that alters that route, or an image that does not auto-install Drupal.
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#configure-probes
##
livenessProbe:
  enabled: true
  path: /
  initialDelaySeconds: 600
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 5
  successThreshold: 1
readinessProbe:
  enabled: true
  path: /
  initialDelaySeconds: 30
  periodSeconds: 5
  timeoutSeconds: 1
  failureThreshold: 5
  successThreshold: 1

## Custom Liveness probe
##
customLivenessProbe: {}

## Custom Readiness probe
##
customReadinessProbe: {}

## lifecycleHooks for the container to automate configuration before or after startup.
##
lifecycleHooks:

## Pod annotations
## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
##
podAnnotations: {}

## Pod extra labels
## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
##
podLabels: {}

## Prometheus Exporter / Metrics
##
metrics:
  enabled: false
  image:
    registry: docker.io
    repository: bitnami/apache-exporter
    tag: 0.8.0-debian-10-r286
    pullPolicy: IfNotPresent
    ## Optionally specify an array of imagePullSecrets.
    ## Secrets must be manually created in the namespace.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    ##
    pullSecrets:
    #   - myRegistryKeySecretName
  ## Metrics exporter resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ##
  # resources: {}
  ## Metrics exporter pod Annotation and Labels
  ##
  podAnnotations:
    prometheus.io/scrape: "true"
    prometheus.io/port: "9117"

# Add custom certificates and certificate authorities to redmine container
certificates:
  customCertificate:
    certificateSecret: ""
    chainSecret: {}
    # name: secret-name
    # key: secret-key
    certificateLocation: /etc/ssl/certs/ssl-cert-snakeoil.pem
    keyLocation: /etc/ssl/private/ssl-cert-snakeoil.key
    chainLocation: /etc/ssl/certs/mychain.pem
  customCAs: []
  ## Override container command
  ##
  command:
  ## Override container args
  ##
  args:
  # - secret: custom-CA
  # - secret: more-custom-CAs
  ## An array to add extra env vars
  ##
  extraEnvVars: []

  ## ConfigMap with extra environment variables
  ##
  extraEnvVarsCM:

  ## Secret with extra environment variables
  ##
  extraEnvVarsSecret:

  image:
    registry: docker.io
    repository: bitnami/minideb
    tag: buster
    ## Specify a imagePullPolicy
    ## Defaults to 'Always' if image tag is 'latest', else set to 'IfNotPresent'
    ## ref: http://kubernetes.io/docs/user-guide/images/#pre-pulling-images
    ##
    pullPolicy: IfNotPresent
    # pullPolicy:
    pullSecrets: []
    #   - myRegistryKeySecretName

## Array with extra yaml to deploy with the chart. Evaluated as a template
##
extraDeploy: []
